const yargs = require('yargs');

const beeng = require('./beeng.js');
const a3manga = require('./a3manga.js');
const hamtruyen = require('./hamtruyen.js')

const pluginModules = [ 
  beeng,
  a3manga,
  hamtruyen
];

const plugIn = (yargs, mod) => mod.plug(yargs);
const plugAll = (yargs, mods) => mods.reduce(plugIn, yargs);

const cli = plugAll(
  yargs
    .scriptName("node feed.js")
    .usage('$0 <cmd> [args]')
    .help(),
  pluginModules
);

const run = () => cli.argv;

run();
