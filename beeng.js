const fetch = require('node-fetch');
const cheerio = require('cheerio');
// const sequelize = require('sequelize');

const mangaCommandBase = require('./mangaCommandBase.js');

const commandArgs = mangaCommandBase({
    command: 'beeng',
    domain: 'beeng.net',
    exampleLink: 'https://beeng.net/truyen-tranh-online/nhat-ta-nguyet-ma-15273/',
    exampleSlug: 'nhat-ta-nguyet-ma-15273',
    oneFetch,
    listFetch,
    searchFetch
})

async function searchFetch(str) {
    const res = await fetch(`https://beeng.net/autosearch/?query=${encodeURI(str)}`);
    const json = await res.json();
    return json.suggestions.map(({ value, slug, id }) => ({ name: value, link: `https://beeng.net/truyen-tranh-online/${slug}-${id}` }));
};

async function listFetch(number) {
    const res = await fetch('https://beeng.net/the-loai');
    const text = await res.text();
    const $ = cheerio.load(text);
    const a = $('.tit').toArray().slice(0, number).map(div => $(div).children('a'));
    const parsed = a.map(a1 => ({
        name: $(a1).text(),
        link: $(a1).attr('href')
    }));
    return parsed;
};

async function oneFetch(slug, number) {
    const res = await fetch(`https://beeng.net/truyen-tranh-online/${slug}`);
    if (!res.ok) return null;
    const text = await res.text();
    const $ = cheerio.load(text);
    const title = $(".sub-bor").first().text().trim();
    const chapters = $(".u84ho3").children('a').toArray().slice(0, number).map(el => ({
        name: $(el).text(),
        link: $(el).attr('href')
    }));
    return {
        title,
        chapters,
        slug
    };
}

module.exports = {
    plug: yargs => yargs.command(...commandArgs)
};
