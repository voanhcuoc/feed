const chalk = require('chalk');

module.exports = ({ command, domain, exampleLink, exampleSlug, listFetch, oneFetch, searchFetch }) => ([
    `${command} [slug]`,
    `${domain} CLI`,
    (yargs) => {
        yargs.positional('slug', {
          type: 'string',
          describe: `slug of the manga, for example:\nlink: ${exampleLink}\nslug: ${exampleSlug}`
        });
        yargs.option('number', {
            alias: 'n',
            default: 10,
            type: 'number',
            describe: 'limit the number of chapters to show'
        });
        yargs.option('list', {
            alias: 'l',
            default: false,
            type: 'boolean',
            describe: 'list all manga'
        });
        yargs.command(
            'search [search]',
            'Search with keywords',
            (yargs) => {yargs.positional('search', { type: 'string' })},
            async ({ search: searchTerm }) => {
                const mangas = await searchFetch(searchTerm);
                console.log(chalk`{cyanBright.bold Search Result}`);
                const padLength = Math.max(...(mangas.map(manga => manga.name.length))) + 5;
                mangas.map(({ name, link }) => {
                    console.log(chalk`{whiteBright ${name.padEnd(padLength, '_')}} ${link}`);
                });
            }
        );
    },
    async function ({ slug, number: inputNumber, list }) {
        const number = (inputNumber > 0) ? inputNumber : undefined;
        if (list) {
            const mangas = await listFetch(number);
            console.log(chalk`{cyanBright.bold List Manga}`);
            const padLength = Math.max(...(mangas.map(manga => manga.name.length))) + 5;
            mangas.map(({ name, link }) => {
                console.log(chalk`{whiteBright ${name.padEnd(padLength, '_')}} ${link}`);
            });
            return;
        };
        if (!slug) {
            console.log('You must provide a slug');
            return;
        };
        const manga = await oneFetch(slug, number);
        if (manga) {
            console.log(chalk`{cyanBright.bold Manga:} {bold ${manga.title}}`);
            console.log(chalk`{cyan slug:} ${manga.slug}`);
            console.log(''); // 1 newline
            const padLength = Math.max(...(manga.chapters.map(chapter => chapter.name.length))) + 5;
            manga.chapters.map(({ name, link }) => {
                console.log(chalk`{whiteBright ${name.padEnd(padLength, '_')}} ${link}`);
            });
        } else {
            console.log(chalk.red('Wrong slug. Can fix.'));
        };
    }
]);