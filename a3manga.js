const fetch = require('node-fetch');
const cheerio = require('cheerio');
// const sequelize = require('sequelize');
const url = require('url');

const mangaCommandBase = require('./mangaCommandBase.js');

const commandArgs = mangaCommandBase({
    command: 'a3manga',
    domain: 'a3manga.com',
    exampleLink: 'https://www.a3manga.com/truyen-tranh/thong-linh-phi/',
    exampleSlug: 'thong-linh-phi',
    oneFetch,
    listFetch,
    searchFetch
})

async function searchFetch(str) {
    const res = await fetch(`https://www.a3manga.com/wp-admin/admin-ajax.php?action=autocompleteCallback&term=${str.replace(' ','+')}`);
    const json = await res.json();
    return json.results.map(({ title, url }) => ({ name: title, link: url }));
}

async function listFetch(number) {
    const res = await fetch('https://www.a3manga.com/danh-sach-truyen/');
    const text = await res.text();
    const $ = cheerio.load(text);
    const table = $('.comic-list-table');
    const trs = $(table).children('tbody').children('tr');
    const a = trs.toArray().slice(0, number).map(tr => $(tr).children('td:nth-child(2)')).map(td => $(td).children('a'));
    const parsed = a.map(a1 => ({
        name: $(a1).text(),
        link: $(a1).attr('href')
    }));
    return parsed;
};

async function oneFetch(slug, number) {
    const res = await fetch(`https://www.a3manga.com/truyen-tranh/${slug}`);
    if (!res.ok) {
        const fixedSlug = await autofixSlug(slug, number);
        if (!fixedSlug) return null;
        return await oneFetch(fixedSlug, number);
    };
    const text = await res.text();
    const $ = cheerio.load(text);
    const title = $(".info-title").text();
    const chapters = $(".chapter-link").toArray().slice(0, number).map(el => ({
        name: $(el).text(),
        link: $(el).attr('href')
    }));
    return {
        title,
        chapters,
        slug
    };
}

async function autofixSlug(slug, number) {
    const res = await fetch(`https://www.a3manga.com/${slug}`, { redirect: 'manual' });
    if (res.status === 301) {
        const chapSlug = url.parse(res.headers.get('location')).pathname;
        const fixedSlug = chapSlug.slice(1, chapSlug.indexOf('-chap'));
        if (fixedSlug === 'truyen-tranh') return null;
        return fixedSlug;
    } else {
        return null;
    }
}

module.exports = {
    plug: yargs => yargs.command(...commandArgs)
};
