const fetch = require('node-fetch');
const cheerio = require('cheerio');
// const sequelize = require('sequelize');
const url = require('url');

const mangaCommandBase = require('./mangaCommandBase.js');

const commandArgs = mangaCommandBase({
    command: 'hamtruyen',
    domain: 'hamtruyen.com',
    exampleLink: 'https://hamtruyen.com/song-tu-dao-lu-cua-toi-0.html',
    exampleSlug: 'song-tu-dao-lu-cua-toi-0',
    oneFetch,
    listFetch,
    searchFetch
})

async function searchFetch(str) {
    const res = await fetch(`https://hamtruyen.com/${str.replace(' ', '-')}/tim-kiem.html`, {
        headers: {
            Referer: 'https://hamtruyen.com/'
        }
    });
    const text = await res.text();
    const $ = cheerio.load(text);
    const a = $('.item_truyennendoc').toArray().map(div => $(div).children('a'));
    const parsed = a.map(a1 => ({
        name: $(a1).text(),
        link: `https://hamtruyen.com${$(a1).attr('href')}`
    }));
    return parsed;
}

async function listFetch(number) {
    const res = await fetch('https://hamtruyen.com/danhsach/index.html');
    const text = await res.text();
    const $ = cheerio.load(text);
    const a = $('.item_truyennendoc').toArray().slice(0, number).map(div => $(div).children('a'));
    const parsed = a.map(a1 => ({
        name: $(a1).text(),
        link: `https://hamtruyen.com${$(a1).attr('href')}`
    }));
    return parsed;
};

async function oneFetch(slug, number) {
    const res = await fetch(`https://hamtruyen.com/${slug}.html`);
    if (!res.ok) return null;
    const text = await res.text();
    const $ = cheerio.load(text);
    const title = $(".tentruyen").text();
    const chapters = $(".tenChapter")
        .toArray().slice(1).slice(0, number).map(div => $(div).children('a'))
        .map(el => ({
            name: $(el).text(),
            link: $(el).attr('href')
        }));
    return {
        title,
        chapters,
        slug
    };
}

module.exports = {
    plug: yargs => yargs.command(...commandArgs)
};
